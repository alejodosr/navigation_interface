#ifndef _NAVIGATION_INTERFACE_ROS_MODULE_H_
#define _NAVIGATION_INTERFACE_ROS_MODULE_H_

//////////////////////////////////////////////////////
///  DroneNavigationStackROSModule.h
///
///  Created on: May 17, 2016
///      Author: alejodosr
///
///  Last modification on: Aug 11, 2016
///      Author: alejodosr
///
//////////////////////////////////////////////////////
#include <ros/ros.h>


//Drone module
#include "droneModuleROS.h"

//I/O stream
//std::cout
#include <iostream>

//Vector
//std::vector
#include <vector>

//String
//std::string, std::getline()
#include <string>

//String stream
//std::istringstream
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>

//String stream
//std::istringstream
#include <sstream>

#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>
#include <std_msgs/Bool.h>


//Ground Robots: Messages out
#include <droneMsgsROS/droneYawRefCommand.h>
#include <droneMsgsROS/droneSpeeds.h>

#include <droneMsgsROS/dronePitchRollCmd.h>
#include <droneMsgsROS/droneDYawCmd.h>
#include <droneMsgsROS/droneDAltitudeCmd.h>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/dronePositionRefCommandStamped.h>
#include <nav_msgs/Odometry.h>
#include "geometry_msgs/PoseStamped.h"

#include "tf/transform_datatypes.h"
#include "tf/transform_broadcaster.h"
#include "tf/transform_listener.h"

#include "sensor_msgs/LaserScan.h"
#include <fstream>
#include <iostream>

#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/bind.hpp>

#define DEBUG_MODE

/////////////////////////////////////////
// Class NavigationInterface
//
//   Description
//
/////////////////////////////////////////
class NavigationInterface : public DroneModule
{

private:
    // Constant variables
    const double THREAD_FREQ = 10; // Hz
    const double MONITOR_FREQ = 0.5; // Hz
    const double TIMEOUT_VALUE = 30; // Seconds
    const double YAW_TOL = 0.1;
    const double TOL_REACHED_POINTS = 0.2;

    // Trajectory related
    nav_msgs::Path current_traj_;
    int current_index_;
    nav_msgs::Odometry current_point_;
    geometry_msgs::PoseStamped current_goal_;
		int goal_id_;

    // Thread related
    boost::mutex traj_mut_, point_mut_, yaw_thread_mut_, traj_thread_mut_, thread_id_mut_, goal_id_mut_, timeout_mut_;
    boost::thread *thread_yaw_id_, *thread_traj_id_, *thread_timeout_;
    bool yaw_thread_active_, traj_thread_active_, goal_not_solved_;
    boost::thread::id thread_id_;

    // Yaw related
    double current_yaw_;
    double reference_yaw_;

    // Timeout monitor related
    bool enable_timeout_monitor_;
    boost::posix_time::ptime initial_time_;

    void setTimeout(bool b){
        timeout_mut_.lock();
        enable_timeout_monitor_ = b;
        initial_time_ = boost::posix_time::second_clock::local_time();
        timeout_mut_.unlock();
    }

    bool isTimeoutEnabled(){
        bool b;
        timeout_mut_.lock();
        b = enable_timeout_monitor_;
        timeout_mut_.unlock();
        return b;
    }

    void setTrajectory(nav_msgs::Path c){
        traj_mut_.lock();
        current_traj_ = c;
        current_index_ = 0;
        traj_mut_.unlock();
    }

    void getTrajectory(nav_msgs::Path &c){
        traj_mut_.lock();
        c = current_traj_;
        traj_mut_.unlock();
    }

    void getNextPointTraj(nav_msgs::Odometry &point, int &curr_i){
        traj_mut_.lock();
        point.pose.pose.position.x = current_traj_.poses[current_index_].pose.position.x;
        point.pose.pose.position.y = current_traj_.poses[current_index_].pose.position.y;
        point.pose.pose.position.z = current_traj_.poses[current_index_].pose.position.z;
        current_index_++;
        if (current_index_ == current_traj_.poses.size())
                current_index_ = -1;
        curr_i = current_index_;
        traj_mut_.unlock();
    }

    void setCurrentGoal(geometry_msgs::PoseStamped p){
        point_mut_.lock();
        current_goal_ = p;
        point_mut_.unlock();
    }

    void getCurrentGoal(geometry_msgs::PoseStamped &p){
        point_mut_.lock();
        p = current_goal_;
        point_mut_.unlock();
    }

    void setCurrentPoint(nav_msgs::Odometry p){
        point_mut_.lock();
        current_point_ = p;
        point_mut_.unlock();
    }

    void getGoalId(int &i, bool &b){
        goal_id_mut_.lock();
        i = goal_id_;
        b = goal_not_solved_;
        goal_id_mut_.unlock();
    }

    void setGoalId(bool b, int request_id){
        goal_id_mut_.lock();
        if ((request_id == goal_id_) || (request_id == 0)){
            goal_id_++;
            goal_not_solved_ = b;
        }
        goal_id_mut_.unlock();
    }

    void getCurrentPoint(nav_msgs::Odometry &p){
        point_mut_.lock();
        p = current_point_;
        point_mut_.unlock();
    }

    bool isYawThreadActive(){
        bool b;
        yaw_thread_mut_.lock();
        b = yaw_thread_active_;
        yaw_thread_mut_.unlock();

        return b;
    }

    void setYawSetActive(bool b){
        yaw_thread_mut_.lock();
        yaw_thread_active_ = b;
        yaw_thread_mut_.unlock();
    }

    bool isTrajThreadActive(){
        bool b;
        traj_thread_mut_.lock();
        b = traj_thread_active_;
        traj_thread_mut_.unlock();

        return b;
    }

    void setTrajSetActive(bool b){
        traj_thread_mut_.lock();
        traj_thread_active_ = b;
        traj_thread_mut_.unlock();
    }

    void setThreadId(boost::thread::id t){
        thread_id_mut_.lock();
        thread_id_ = t;
        thread_id_mut_.unlock();
    }

    void publishGoalReached(boost::thread::id t, bool b){
        thread_id_mut_.lock();
        if (t == thread_id_){
            std_msgs::Bool goal;
            goal.data = b;
            goal_reached_pub_.publish(goal);
        }
        thread_id_mut_.unlock();
    }

protected:
    //Subscriber
    ros::Subscriber droneCmdVelSub;
    ros::Subscriber droneOdomSub;
    ros::Subscriber droneTrajSub;
    ros::Subscriber droneScanSub;
    ros::Subscriber droneResetSub;
    ros::Subscriber droneGoalSub;
    ros::Subscriber droneGlobalSub;
    ros::Publisher yaw_pub_;
    ros::Publisher obstacle_pub_;
    ros::Publisher goal_reached_pub_;
    ros::Publisher goal_pub_;
    ros::Publisher traj_pub_;

    void droneOdomCallback(const nav_msgs::Odometry &msg);
    void droneTrajCallback(const  nav_msgs::Path &msg);
    void droneScanCallback(const sensor_msgs::LaserScan &msg);
    void droneGlobalCallback(const geometry_msgs::PoseStamped &msg);
    void publishYaw(droneMsgsROS::droneYawRefCommand yaw);
    void yawMonitorThread_();
    void trajMonitorThread_();
    void timeoutMonitorThread_();


public:
    NavigationInterface(void);
    ~NavigationInterface(void);
    void open(ros::NodeHandle & nIn);
    void close();

protected:
    bool init();
    bool resetValues();
    bool startVal();
    bool stopVal();

public:
    bool run();

};

#endif


