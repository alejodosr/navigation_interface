//////////////////////////////////////////////////////
///  NavigationInterfaceSource.cpp
///
///  Created on: May 14, 2016
///      Author: alejodosr
///
///  Last modification on: Aug 11, 2016
///      Author: alejodosr
///
//////////////////////////////////////////////////////


#include "navigation_interface.h"

void NavigationInterface::timeoutMonitorThread_(){
    while(1){
        // Check whether timeout has to be generated
        boost::posix_time::time_duration diff = boost::posix_time::second_clock::local_time() - initial_time_;
#ifdef DEBUG_MODE
    std::cout << "DEBUG_MSG: Total in seconds: " << diff.total_milliseconds() / 1000.d << std::endl;
#endif
        if ((diff.total_milliseconds() > (TIMEOUT_VALUE * 1000) && isTimeoutEnabled())){
            // Re-send previous goal to move_base
            geometry_msgs::PoseStamped goal;
            getCurrentGoal(goal);
            goal.header.stamp = ros::Time::now();
            goal.header.frame_id = "map";

            goal.pose.orientation.x = 0;
            goal.pose.orientation.y = 0;
            goal.pose.orientation.z = 0;
            goal.pose.orientation.w = 1;

            // Publish the subgoal
            goal_pub_.publish(goal);

            // Restart timeout
            setTimeout(true);

#ifdef DEBUG_MODE
    std::cout << "DEBUG_MSG: Timeout reached, resending goal to move base node" << std::endl;
#endif
        }

        // Set yaw monitor frequency
        boost::this_thread::sleep(boost::posix_time::milliseconds((1.d/MONITOR_FREQ) * 1000));
    }
}

void NavigationInterface::yawMonitorThread_(){
    // Set as active
    setYawSetActive(true);

    // Send reference to the controller
    droneMsgsROS::droneYawRefCommand final_yaw;
    final_yaw.header.stamp = ros::Time::now();
    final_yaw.yaw = reference_yaw_;
    yaw_pub_.publish(final_yaw);

    // Adjust angle in order to be able to compare
    double ref_yaw;
    if (reference_yaw_ < 0) ref_yaw += 2 * M_PI;
    else    ref_yaw = reference_yaw_;

    while(fabs(reference_yaw_ - current_yaw_) > YAW_TOL){
        // Set yaw monitor frequency
        boost::this_thread::sleep(boost::posix_time::milliseconds((1.d/THREAD_FREQ) * 1000));
    }

#ifdef DEBUG_MODE
    std::cout << "DEBUG_MSG: Yaw reference reached (curr: " << current_yaw_ << " ref: " << reference_yaw_ << ")" << std::endl;
#endif

    // Trigger trajectory thread
    thread_traj_id_ = new boost::thread(boost::bind(&NavigationInterface::trajMonitorThread_, this));
    setThreadId(thread_traj_id_->get_id());
}

void NavigationInterface::trajMonitorThread_(){
		// Read goal id
		int id;
		bool b;
		getGoalId(id, b);

    // Set as active
    setTrajSetActive(true);

    // Send trajectory
    droneMsgsROS::dronePositionTrajectoryRefCommand trajectory_reference_msg;
    trajectory_reference_msg.header.stamp = ros::Time::now();
    trajectory_reference_msg.initial_checkpoint = 0;
    trajectory_reference_msg.is_periodic = false;

    nav_msgs::Path curr_traj;
    getTrajectory(curr_traj);

    // Get next goal
    bool goal_reached = false;
    geometry_msgs::PoseStamped curr_goal;
    getCurrentGoal(curr_goal);

    for(int i=0; i<curr_traj.poses.size(); i++){
        droneMsgsROS::dronePositionRefCommand next_waypoint;
        next_waypoint.x = curr_traj.poses[i].pose.position.x;
        next_waypoint.y = curr_traj.poses[i].pose.position.y;
        next_waypoint.z = curr_goal.pose.position.z;
        trajectory_reference_msg.droneTrajectory.push_back(next_waypoint);
    }

#ifdef DEBUG_MODE
    std::cout << "DEBUG_MSG: goal x: " << curr_traj.poses.back().pose.position.x << " y: " << curr_traj.poses.back().pose.position.y << std::endl;
#endif

    traj_pub_.publish(trajectory_reference_msg);

    // Get curr point
    nav_msgs::Odometry curr_point;

    while(!goal_reached){
        // Get current point
        getCurrentPoint(curr_point);

        if ((fabs(curr_goal.pose.position.x - curr_point.pose.pose.position.x) < TOL_REACHED_POINTS)
                && (fabs(curr_goal.pose.position.y - curr_point.pose.pose.position.y) < TOL_REACHED_POINTS)
                && (fabs(curr_goal.pose.position.z - curr_point.pose.pose.position.z) < TOL_REACHED_POINTS)){
            goal_reached = true;
        }

        // Tell GMP that you reached the goal
        publishGoalReached(boost::this_thread::get_id(), false);

        // Set yaw monitor frequency
        boost::this_thread::sleep(boost::posix_time::milliseconds((1.d/THREAD_FREQ) * 1000));
    }

#ifdef DEBUG_MODE
    std::cout << "DEBUG_MSG: Global goal reached" << std::endl;
#endif

    // Tell GMP that you reached the goal
    publishGoalReached(boost::this_thread::get_id(), true);

    // The goal is solved
    setGoalId(false, id);

}

NavigationInterface::NavigationInterface(void){
}

NavigationInterface::~NavigationInterface(void){
    // Stop timeout thread
    thread_timeout_->interrupt();
    thread_timeout_->detach();
}

void NavigationInterface::droneGlobalCallback(const geometry_msgs::PoseStamped &msg){
    // Convert yaw
    tf::Quaternion q(msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w);
    tf::Matrix3x3 m(q);

    //convert quaternion to euler angels
    double y, p, r;
    m.getEulerYPR(y, p, r);

    // From -M_PI to M_PI
    reference_yaw_ = y;

#ifdef DEBUG_MODE
    std::cout << "DEBUG_MSG: Yaw reference: " << y << std::endl;
    std::cout << "DEBUG_MSG: Received in topic - x: " << msg.pose.position.x << " y: " << msg.pose.position.y << " z: " << msg.pose.position.z << std::endl;
#endif

    // Store last goal
    setCurrentGoal(msg);

    // Send goal
    geometry_msgs::PoseStamped goal;
    goal.header.stamp = ros::Time::now();
    goal.header.frame_id = "map";
    goal.pose.position.x = msg.pose.position.x;
    goal.pose.position.y = msg.pose.position.y;
    goal.pose.position.z = msg.pose.position.z;

    goal.pose.orientation.x = 0;
    goal.pose.orientation.y = 0;
    goal.pose.orientation.z = 0;
    goal.pose.orientation.w = 1;

    // Publish the subgoal
    goal_pub_.publish(goal);

    // Specify goal not solved
    setGoalId(true, 0);

    // Start timeout
    setTimeout(true);
}

void NavigationInterface::droneTrajCallback(const nav_msgs::Path &msg){
		int i;
		bool goal_not_solved;
		getGoalId(i, goal_not_solved);
    if (msg.poses.size() > 0 && goal_not_solved){

        // Interrupt threads
        if (isYawThreadActive()){
            thread_yaw_id_->interrupt();
            thread_yaw_id_->detach();
            setYawSetActive(false);

#ifdef DEBUG_MODE
        std::cout << "DEBUG_MSG: Killing yaw thread" << std::endl;
#endif
        }
        if (isTrajThreadActive()){
            thread_traj_id_->interrupt();
            thread_traj_id_->detach();
            setTrajSetActive(false);

#ifdef DEBUG_MODE
        std::cout << "DEBUG_MSG: Killing traj thread" << std::endl;
#endif
        }

        // Store trajectory
        setTrajectory(msg);

        // Start thread
        geometry_msgs::PoseStamped curr_goal;
        getCurrentGoal(curr_goal);
        if (curr_goal.pose.orientation.x == 0 &&
                curr_goal.pose.orientation.y == 0 &&
                curr_goal.pose.orientation.z == 0 &&
                curr_goal.pose.orientation.w == 1){
            thread_traj_id_ = new boost::thread(boost::bind(&NavigationInterface::trajMonitorThread_, this));
            setThreadId(thread_traj_id_->get_id());
        }
        else
            thread_yaw_id_ = new boost::thread(boost::bind(&NavigationInterface::yawMonitorThread_, this));

#ifdef DEBUG_MODE
        std::cout << "DEBUG_MSG: Thread started" << std::endl;
#endif
    }
    else{
        std_msgs::Bool obs;
        obs.data = true;
        obstacle_pub_.publish(obs);
    }
}

void NavigationInterface::droneOdomCallback(const nav_msgs::Odometry &msg){
    // Storing new yaw value
    tf::Quaternion q(msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z, msg.pose.pose.orientation.w);
    tf::Matrix3x3 m(q);

    // Convert quaternion to euler angles
    double y, p, r;
    m.getEulerYPR(y, p, r);
    current_yaw_ = y;
    if (current_yaw_ < 0)  current_yaw_ += 2 * M_PI;  //No angles lower < 0

    // Store current point
    setCurrentPoint(msg);
}

void NavigationInterface::droneScanCallback(const sensor_msgs::LaserScan &msg){
}

void NavigationInterface::open(ros::NodeHandle & nIn)
{
    //Node
    DroneModule::open(nIn);


    //Init
    if(!init())
    {
        std::cout << "Error init" << std::endl;
        return;
    }

    //Subscribers
    droneOdomSub = nIn.subscribe("odometry/filtered", 1, &NavigationInterface::droneOdomCallback, this);
    droneTrajSub = nIn.subscribe("move_base/NavfnROS/plan", 1, &NavigationInterface::droneTrajCallback, this);
    droneScanSub = nIn.subscribe("scan", 1, &NavigationInterface::droneScanCallback, this);
    droneGlobalSub   = nIn.subscribe("globalGoal", 1, &NavigationInterface::droneGlobalCallback, this);

    //Publishers
    yaw_pub_ = nIn.advertise<droneMsgsROS::droneYawRefCommand>("droneControllerYawRefCommand", 1, true);
    obstacle_pub_   = nIn.advertise<std_msgs::Bool>("obstacle_found", 1);
    goal_reached_pub_   = nIn.advertise<std_msgs::Bool>("goal_reached", 1);
    goal_pub_ = nIn.advertise<geometry_msgs::PoseStamped>("move_base_simple/goal", 1);
    traj_pub_ = nIn.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>("droneTrajectoryAbsRefCommand", 1);

    // Init variables
    reference_yaw_ = 0;
    yaw_thread_active_ = false;
    traj_thread_active_ =  false;
    goal_not_solved_ = false;
    goal_id_ = 1;
    setTimeout(false);
}


bool NavigationInterface::stopVal(){
    // Stop timeout thread
    thread_timeout_->interrupt();
    thread_timeout_->detach();
}

bool NavigationInterface::run(){
    if(!DroneModule::run())
        return false;

    if(droneModuleOpened==false)
        return false;

    return true;
}

bool NavigationInterface::startVal(){
    // Generate timeout thread
    thread_timeout_ = new boost::thread(boost::bind(&NavigationInterface::timeoutMonitorThread_, this));

#ifdef DEBUG_MODE
    std::cout << "DEBUG_MSG: Tiemout thread generated" << std::endl;
#endif


    //End
    return DroneModule::startVal();
}

bool NavigationInterface::resetValues()
{
    return true;
}

void NavigationInterface::close()
{
    DroneModule::close();



    //Do stuff

    return;
}

bool NavigationInterface::init()
{
    DroneModule::init();

    return true;
}






