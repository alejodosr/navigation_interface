//////////////////////////////////////////////////////
///  NavigationInterfacess_SourceNode.h
///
///  Created on: May 14, 2016
///      Author: alejodosr
///
///  Last modification on: May 14, 2016
///      Author: alejodosr
///
//////////////////////////////////////////////////////



//I/O Stream
//std::cout
#include <iostream>

#include <string>


// ROS
//ros::init(), ros::NodeHandle, ros::ok(), ros::spinOnce()
#include "ros/ros.h"



//ROSModule
#include "navigation_interface.h"

using namespace std;

int main(int argc, char **argv)
{
    //Init
    ros::init(argc, argv, "NonInitializedNode"); //Say to ROS the name of the node and the parameters
    ros::NodeHandle n; //Este nodo admite argumentos!!

    std::string node_name=ros::this_node::getName();
    cout<<"node name="<<node_name<<endl;

    //Class definition
    NavigationInterface NavigationStackROSModule;

    //Open!
    NavigationStackROSModule.open(n);


    //Loop -> Ashyncronous Module
    while(ros::ok())
    {
        ros::spin();
    }

    return 1;
}

